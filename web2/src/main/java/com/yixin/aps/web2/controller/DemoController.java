package com.yixin.aps.web2.controller;

import com.yixin.aps.common.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by maming on 2018/5/23.
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    @RequestMapping("/hello")
    public PageList index() throws Exception {
        PageList<String> pageList =new PageList<String>(1,1,1);
        return pageList;
    }
    @RequestMapping("/hello1")
    public String index2()
    {
        return "Hello World12";
    }
}
