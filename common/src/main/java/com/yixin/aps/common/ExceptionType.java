package com.yixin.aps.common;

public class ExceptionType {

    //Status 数字 操作状态： 0:成功 1:失败 2:错误 7:未登录
    /**
     * 0成功
     */
    public static final int Success = 0;

    /**
     * 1失败
     */
    public static final int Fail = 1;

    /**
     * 2错误
     */
    public static final int Error = 2;

    /**
     * 7:未登录
     */
    public static final int NoLogin = 7;

    /**
     * -108IP未在白名单
     */
    public static final int IPError = -108;

    /***
     * -111未授权
     */
    public static final int NotAuthorize = -111;

    /**
     * -112 无效的参数信息
     */
    public static final int ArgumentError = -112;
    /**
     * -113 无效的渠道名称信息
     */
    public static final int ChannelError = -113;

    /**
     * 行驶里程输入不合理
     */
    public static final int MileageInValid = -201;

    /**
     * 选择的初登日期不合理
     */
    public static final int CarRegDateInValid = -202;
    /**
     * 选择的城市暂未开通定价功能
     */
    public static final int CityNoOpen = -203;

    /**
     * 过户次数超出限制
     */
    public static final int OverMaxTransferTime = -204;

    /**
     * 输入的车款ID不存在
     */
    public static final int NoHaveVehicleModelID = -205;
    /**
     * 城市ID不合法
     */
    public static final int InValidCityID = -206;
}
