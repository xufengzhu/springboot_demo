package com.yixin.aps.common;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/// <summary>
/// 分页对象
/// </summary>

public class PageList<T>
{
    /**
     * 页面索引值
     */
    //@JsonProperty
    private int PageIndex;
    /**
     * 每页记录的数量
     */
    //@JsonProperty
    private int PageSize;
    /**
     * 记录总条数
     */
    //@JsonProperty
    private int TotalCount;
    /**
     * 总页数
     */
    //@JsonProperty
    private int TotalPages;
    /**
     * 是否存在前一页
     */
    //@JsonProperty
    private boolean HasPreviousPage;
    /**
     * 是否存在下一页
     */
    //@JsonProperty
    private boolean HasNextPage;

    public int getPageIndex() {
        return PageIndex;
    }

    private void setPageIndex(int pageIndex) {
        PageIndex = pageIndex;
    }

    public int getPageSize() {
        return PageSize;
    }

    private void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public int getTotalCount() {
        return TotalCount;
    }

    private void setTotalCount(int totalCount) {
        TotalCount = totalCount;
    }

    public int getTotalPages() {
        return TotalPages;
    }

    private void setTotalPages(int totalPages) {
        TotalPages = totalPages;
    }

    public boolean isHasPreviousPage() {
        return PageIndex > 1;
    }

    public boolean isHasNextPage() {
        return PageIndex < TotalPages;
    }

    public T getSource() {
        return Source;
    }

    public void setSource(T source) {
        Source = source;
    }



    private List<Integer> NumberPage;



    public List<Integer> getNumberPage() {
        return NumberPage;
    }

    public void setNumberPage(List<Integer> numberPage) {
        NumberPage = numberPage;
    }


    /// <summary>
    /// 单前页数据
    /// </summary>
    private T Source;

    /// <summary>
    /// 获取指定的数字型页数
    /// </summary>
    /// <param name="count">需要的数量</param>
    /// <returns>返回分页表</returns>
    public List<Integer> GetNumberPage(Integer count)
    {
        List<Integer> result = new ArrayList<Integer>();
        result.add(PageIndex);

        if (count <= 1)
        {
            return result;
        }

        int preIndex = PageIndex - 1;
        int nextIndex = PageIndex + 1;
        for (int i = 0; i < count - 1; )
        {
            boolean hasData = false;

            if (preIndex >= 1)
            {
                hasData = true;
                result.add(preIndex);
                preIndex = preIndex - 1;
                i++;
                if (i >= count - 1)
                {
                    break;
                }
            }

            if (nextIndex <= TotalPages)
            {
                hasData = true;
                result.add(nextIndex);
                nextIndex = nextIndex + 1;
                i++;
                if (i >= count - 1)
                {
                    break;
                }
            }

            if (!hasData)
            {
                break;
            }
        }

        Collections.sort(result);

        return result;
    }

    /// <summary>
    /// 分页对象
    /// </summary>
    /// <param name="pageIndex">页面索引值
    /// <param name="pageSize">每页记录的数量
    /// <param name="totalCount">记录总条数
    public PageList(int pageIndex, int pageSize, int totalCount) throws Exception
    {
        if (pageSize <= 0)
        {
            throw new Exception("pageSize");
        }

        PageSize = pageSize;


        if (totalCount <= 0)
        {
            //没有记录信息
            TotalCount = 0;
            PageIndex = 1;
            TotalPages = 1;
        }
        else
        {
            TotalCount = totalCount;

            //总页数
            TotalPages = (int)Math.ceil(TotalCount / (double)pageSize);

            //当前页处理
            if (pageIndex <= 0)
            {
                PageIndex = 1;
            }
            else
            {
                if (pageIndex > TotalPages)
                {
                    PageIndex = TotalPages;
                }
                else
                {
                    PageIndex = pageIndex;
                }
            }
        }

        NumberPage = GetNumberPage(5);
    }

    private static int Coparison(int i, int j)
    {
        if (i > j)
        {
            return 1;
        }
        else if (i < j)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }

    public String getPageNumStr(String href)
    {
        if(this.getTotalPages()==1)
            return "";
        StringBuilder str = new StringBuilder();
        int p = 0;
        int currentPage = this.getPageIndex();
        if (currentPage > 2)
            currentPage = currentPage - 2;
        else if (currentPage == 2)
            currentPage = currentPage - 1;
        if (isHasPreviousPage())
        {
            //str.append("<li><a class=\"first\" href=\"" + href + "1\"></a></li> ");//首页
            str.append("<li><a class=\"pre\" href=\"" + href + (getPageIndex() - 1) + ");\">Previous</a></li> ");//上一页
        }
        for (int i = currentPage; i <= getTotalPages(); i++)
        {
            if (i == getPageIndex()) { str.append("<li><a class=\"number current\" href=\"" + href + i + ");\">" + i + "</a></li>"); continue; }
            p++;
            if (p == 5)
                break;
            str.append("<li><a class=\"number\" href=\"" + href+ i + ");\">" + i + "</a></li>");
        }
        if (isHasNextPage())
        {
            str.append("<li><a class=\"nxt\" href=\""+href+ (PageIndex + 1) + ");\">Next</a></li>");//下一页
            //str.append("<li><a class=\"last\" href=\""+href + (TotalPages) + "\"></a></li>");//尾页
        }
        return str.toString();

    }
}
