package com.yixin.aps.common;


/**
 * 封装返回的类
 * @param <T>
 */
public class ReturnJsonType<T> {

	//解决json大小写
	//@JsonProperty
	private int Status;
	//@JsonProperty
	private String Message;
	//@JsonProperty
	private T Data;


	//@JsonIgnore
	public int getStatus() {
		return Status;
	}

	//@JsonIgnore
	public void setStatus(int status) {
		Status = status;
	}

	//@JsonIgnore
	public String getMessage() {
		return Message;
	}

	//@JsonIgnore
	public void setMessage(String message) {
		Message = message;
	}

	//@JsonIgnore
	public T getData() {
		return Data;
	}

	//@JsonIgnore
	public void setData(T data) {
		Data = data;
	}


	
}

