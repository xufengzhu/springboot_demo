package com.yixin.aps.common;

/**
 * Created by Administrator on 2017/8/11.
 * 经纬度
 */
public class Coordinate {
    private double latitude; //维度
    private double longitude; //经度
    private String member; //位置名称
    public static final double radius; //半径

    static {
        radius=10000;
    }
    public Coordinate()
    {
    }

    public Coordinate(double longitude, double latitude, String member)
    {
        this.longitude =longitude;
        this.latitude =latitude;
        this.member=member;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public static double getRadius() {
        return radius;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }
}
