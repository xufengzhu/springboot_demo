package com.yixin.aps.common;

import org.apache.commons.lang.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 时间工具类
 * @author wuwenqiang
 *
 */
public class DateUtil {
	private static String defaultDatePattern = "yyyy-MM-dd HH:mm:ss";
	private static String defaultDisplayDatePattern = "yyyy-MM-dd HH:mm:ss";
	private static String defaultDisplayTimeZone = "Asia/Shanghai";
	private static String defaultDatePatternYmd = "yyyy-MM-dd";
	private static String defaultDatePatternMd = "MM.dd";
	private static String defaultDatePatternMdcn = "MM月dd日";
	private static String displayDatePatternYMDHM = "yyyy-MM-dd HH:mm";
	private static String displayDatePatternYMDHMS = "yyyyMMddHHmmss";
	
	public static String getDatePattern() {
		return defaultDatePatternYmd;
	}
	/**
	 * MM.dd
	 * @param date
	 * @return
	 */
	public static String displayFormatMd(Date date) {
		String returnValue = "";
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat(defaultDatePatternMd);
			df.setTimeZone(TimeZone.getTimeZone(defaultDisplayTimeZone));
			returnValue = df.format(date);
		}
		return (returnValue);
	}
	/**
	 * MM月dd日
	 * @param date
	 * @return
	 */
	public static String displayFormatMdcn(Date date) {
		String returnValue = "";
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat(defaultDatePatternMdcn);
			df.setTimeZone(TimeZone.getTimeZone(defaultDisplayTimeZone));
			returnValue = df.format(date);
		}
		return (returnValue);
	}
	/**
	 * yyyy-MM-dd HH:mm
	 * @param date
	 * @return
	 */
	public static String displayFormatYMDHM(Date date) {
		String returnValue = "";
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat(displayDatePatternYMDHM);
			df.setTimeZone(TimeZone.getTimeZone(defaultDisplayTimeZone));
			returnValue = df.format(date);
		}
		return (returnValue);
	}
	/**
	 * yyyyMMddHHmmss
	 * @param date
	 * @return
	 */
	public static String displayFormatYMDHMS(Date date) {
		String returnValue = "";
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat(displayDatePatternYMDHMS);
			df.setTimeZone(TimeZone.getTimeZone(defaultDisplayTimeZone));
			returnValue = df.format(date);
		}
		return (returnValue);
	}
	
	/**
	 * yyyy-MM-dd
	 * @param date
	 * @return
	 */
	public static String displayFormat(Date date) {
		String returnValue = "";
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat(defaultDatePatternYmd);
			df.setTimeZone(TimeZone.getTimeZone(defaultDisplayTimeZone));
			returnValue = df.format(date);
		}
		return (returnValue);
	}
	
	/**
	 * yyyy-MM
	 * @param date
	 * @return
	 */
	public static String displayFormatYm(Date date)
	{
		String returnValue ="";
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
			df.setTimeZone(TimeZone.getTimeZone(defaultDisplayTimeZone));
			returnValue =df.format(date);
		}
		return returnValue;
	}
	
	/**
	 * yyyy-MM-dd
	 * @param date
	 * @return
	 */
	public static String displayFormatYmd(Date date) {
		String returnValue = "";
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat(defaultDatePatternYmd);
			df.setTimeZone(TimeZone.getTimeZone(defaultDisplayTimeZone));
			returnValue = df.format(date);
		}
		return (returnValue);
	}
	
	
	/**
	 * yyyy-MM-dd HH:mm:ss
	 * @param date
	 * @return
	 */
	public static String displayFormatYmdhs(Date date) {
		String returnValue = "";
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat(defaultDisplayDatePattern);
			df.setTimeZone(TimeZone.getTimeZone(defaultDisplayTimeZone));
			returnValue = df.format(date);
		}
		return (returnValue);
	}

	public static String format(Date date) {
		return format(date, getDatePattern());
	}

	public static String format(Date date, String pattern) {
		String returnValue = "";
		try {
			if (date != null) {
				SimpleDateFormat df = new SimpleDateFormat(pattern);
				returnValue = df.format(date);
			}
		} catch (Exception e) {
			return returnValue;
		}

		return (returnValue);
	}

	/**
	 * 格式化成 yyyy-MM-dd HH:mm:ss
	 * @param strDate
	 * @return
	 * @throws ParseException
	 */
	public static Date parse(String strDate) throws ParseException {
		return parse(strDate, getDatePattern());
	}
    
	
	public static Date parse(String strDate, String pattern)
			throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		return df.parse(strDate);
	}
    
	public static boolean validateDate(String strDate) {
		boolean flag = false;
		try {
			SimpleDateFormat df = new SimpleDateFormat(defaultDatePatternYmd);
			df.parse(strDate);
			flag = true;
		} catch (Exception e) {
			return false;
		}

		return flag;
	}

	/**
	 * 取得yyyy-MM-dd
	 * @param date
	 * @return
	 */
	public static String getDate(Date date) {
		return format(date, "yyyy-MM-dd");
	}
	/**
	 * 取得HH:mm:ss
	 * @param date
	 * @return
	 */
	public static String getTime(Date date) {
		return format(date, "HH:mm:ss");
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date simpleDate(Date date) {
		String result = "";
		Date sdate = new Date(System.currentTimeMillis());
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			result = sdf.format(date);
			sdate = DateUtils.parseDate(result, new String[] { "yyyy/MM/dd",
					"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm" });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sdate;
	}

	public static Date currentDate() {
		TimeZone t = TimeZone.getTimeZone(defaultDisplayTimeZone);
		Locale l = new Locale("zh", "CN");
		Calendar c = Calendar.getInstance(t, l);
		return new Date(c.getTimeInMillis());
	}

	public static long getMillis(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.getTimeInMillis();
	}

	
	public static int getYear(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.YEAR);
	}

	public static int getMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MONTH) + 1;
	}

	public static int getDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.DAY_OF_MONTH);
	}

	public static int getHour(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMinute(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MINUTE);
	}

	public static int getSecond(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.SECOND);
	}

	public static Date addMonth(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, n);
		return cal.getTime();
	}
	public static Date addYear(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, n);
		return cal.getTime();
	}
	public static Date subtractYear(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, n * (-1));
		return cal.getTime();
	}
	public static Date subtractMonth(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, n * (-1));
		return cal.getTime();
	}

	public static Date addHour(Date date, long hour) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) + hour * 3600 * 1000);
		return c.getTime();
	}

	public static Date subtractHour(Date date, long hour) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) - hour * 3600 * 1000);
		return c.getTime();
	}

	public static Date addDay(Date date, int day) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) + ((long) day) * 24 * 3600 * 1000);
		return c.getTime();
	}

	public static Date subtractDay(Date date, int day) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) - ((long) day) * 24 * 3600 * 1000);
		return c.getTime();
	}

	public static Date addSecond(Date date, long second) {
		return new Date(getMillis(date) + second * 1000);
	}

	public static Date subtractSecond(Date date, long second) {
		return new Date(getMillis(date) - second * 1000);
	}

	public static Date addMillis(Date date, long millis) {
		return new Date(getMillis(date) + millis);
	}

	public static Date subtractMillis(Date date, long millis) {
		return new Date(getMillis(date) - millis);
	}

	public static Date getDate(int year, int month, int day) {
		Calendar c = Calendar.getInstance();
		c.set(year, month - 1, day);
		return c.getTime();
	}

	public static boolean isBetween(Date date, Date date1, Date date2) {
		long m = DateUtil.getMillis(date);
		long m1 = DateUtil.getMillis(date1);
		long m2 = DateUtil.getMillis(date2);
		if (m1 <= m && m <= m2) {
			return true;
		}
		return false;
	}


	/**
	 * 取得消息的时间格式
	 * 
	 * @param messageDate
	 * @return
	 */
	public static String getDayMessage(Date messageDate) {
		Date currentDate = new Date();
		SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd");
		String today = format3.format(currentDate);

		String yesterday = format3.format(DateUtil.subtractDay(currentDate, 1));
		String qday = format3.format(DateUtil.subtractDay(currentDate, 2));

		String messageDay = format3.format(messageDate);

		if (messageDay.equalsIgnoreCase(today)) {
			return "今天";
		}
		if (messageDay.equalsIgnoreCase(yesterday)) {
			return "昨天";
		}
		if (messageDay.equalsIgnoreCase(qday)) {
			return "前天";
		}
		return DateUtil.format(messageDate, "yyyy-MM-dd");

	}
	/**
	 * 将字符串转换为日期 yyyy-MM-dd
	 * @param strDate
	 * @return
	 * @throws ParseException
	 */
	public static Date parseStrToDate(String strDate) throws ParseException {
		return parse(strDate, "yyyy-MM-dd");

	}
	/**
	 * 将字符串转换为日期 yyyy-MM-dd HH:mm
	 * @param strDate
	 * @return
	 * @throws ParseException
	 */
	public static Date parseStrToDate2(String strDate) throws ParseException {
		return parse(strDate, "yyyy-MM-dd HH:mm");

	}
	/**
	 * 将字符串转换为日期 yyyy-MM-dd HH:mm:ss
	 * @param strDate
	 * @return
	 * @throws ParseException
	 */
	public static Date parseStrToDate3(String strDate) throws ParseException {
		return parse(strDate, "yyyy-MM-dd HH:mm:ss");

	}
	/**
	 * 将字符串转换为小时
	 * @param strDate
	 * @return
	 * @throws ParseException
	 */
	public static boolean parseStrToHourStr(String strDate) throws ParseException{
		//Pattern.compile("^[0-2]{1}[0-9]{0,1}[:][0-9]{1,2}$");
		strDate=strDate.replaceAll("：",":").replaceAll(":", ":");
		Pattern pattern = Pattern.compile("^(?:(?:[0-2][0-3])|(?:[0-1][0-9])):[0-5][0-9]$");
		Matcher matchName = pattern.matcher(strDate);
		return matchName.find();

    }

	/**
	 * 验证年月日是否合法
	 * @param strDate
	 * @return
	 * @throws ParseException
	 */
  public static boolean validateYear(String strDate) throws ParseException{
	    Pattern pattern = Pattern.compile("^(?:19|20)[0-9][0-9]-(?:(?:0[1-9])|(?:1[0-2]))-(?:(?:[0-2][1-9])|(?:[1-3][0-1]))$");
		Matcher matchName = pattern.matcher(strDate);
		return matchName.find();
  }
	
	/**
	 * 获得当前日期年月日
	 * @return
	 * @throws ParseException 
	 */
	public static long currentDateTime() throws ParseException {
		String result="";
		TimeZone t = TimeZone.getTimeZone(defaultDisplayTimeZone);
		Locale l = new Locale("zh", "CN");
		Calendar c = Calendar.getInstance(t, l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		result = sdf.format( new Date(c.getTimeInMillis()));
		return  DateUtil.getMillis(parseStrToDate(result));
	}

	/**
	 * 获得当前日期小时
	 * @return
	 * @throws ParseException 
	 */
	public static int currentDateHour() throws ParseException {
		TimeZone t = TimeZone.getTimeZone(defaultDisplayTimeZone);
		Locale l = new Locale("zh", "CN");
		Calendar c = Calendar.getInstance(t, l);
		int hour=c.get(Calendar.HOUR_OF_DAY);
		return hour;
	}
	/**
	 * 判断字符串是否是数字
	 * @return
	 * @throws ParseException 
	 * */
	public static boolean isNumeric(String str){  
	    Pattern pattern = Pattern.compile("[0-9]*");  
	    return pattern.matcher(str).matches();     
	} 
	
	/**
	 * 获得当前日期分钟
	 * @return
	 * @throws ParseException 
	 */
	public static int currentDateMinute() throws ParseException {
		TimeZone t = TimeZone.getTimeZone(defaultDisplayTimeZone);
		Locale l = new Locale("zh", "CN");
		Calendar c = Calendar.getInstance(t, l);
		int minute=c.get(Calendar.MINUTE);
		return minute;
	}
	
    /**
	 * 获得当前日期的前一天
	 * @return
	 * @throws ParseException 
	 */
	public static String currentTime() throws ParseException {
		TimeZone t = TimeZone.getTimeZone(defaultDisplayTimeZone);
		Locale l = new Locale("zh", "CN");
		Calendar c = Calendar.getInstance(t, l);
		c.add(Calendar.DAY_OF_YEAR, 1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return  sdf.format( new Date(c.getTimeInMillis()));
	}
	
	public static int getCurrentHour(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.HOUR_OF_DAY-1);
	}

	/**
	 *  获取当月第一天
	 */
	public static Date getMonthFirstDay(){

		Calendar cal = Calendar.getInstance();
		Calendar f = (Calendar)cal.clone();
		f.clear(); 
		f.set(Calendar.YEAR,cal.get(Calendar.YEAR)); 
		f.set(Calendar.MONTH,cal.get(Calendar.MONTH)); 
		f.set(Calendar.DATE,1);
		f.set(Calendar.HOUR,0);
		f.set(Calendar.MINUTE,0);
		f.set(Calendar.SECOND,0);
		f.set(Calendar.MILLISECOND,0);
		return f.getTime(); 
	}
	 
	/**
	 * 获取当月最后一天
	 */
	public static Date getMonthLastDay(){
		   Calendar cal = Calendar.getInstance();
		   Calendar l = (Calendar)cal.clone();
		   l.clear();
		   l.set(Calendar.YEAR,cal.get(Calendar.YEAR));
		   l.set(Calendar.MONTH,cal.get(Calendar.MONTH)+1);
		   l.set(Calendar.MILLISECOND,-1); 
		   l.set(Calendar.DATE,0);
		   l.set(Calendar.HOUR,0);
		   l.set(Calendar.SECOND,0);
		   l.set(Calendar.MILLISECOND,0);
		   return l.getTime();
	}
	
	/**
	 * 获取Integer类型年月日 yyyyMMdd
	 */
	public static Integer displayFormatYmdInt(Date date)
	{
		int returnValue =19000101;
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
			df.setTimeZone(TimeZone.getTimeZone(defaultDisplayTimeZone));
			returnValue =Integer.parseInt(df.format(date));
		}
		return returnValue;
	}
}
