package com.yixin.aps.web.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yixin.aps.common.ExceptionType;
import com.yixin.aps.common.PageList;
import com.yixin.aps.common.ReturnJsonType;
import com.yixin.aps.entity.SysContent;
import com.yixin.aps.service.impl.ISysContentServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by maming on 2018/5/23.
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    private Logger logger = LogManager.getLogger(DemoController.class);

    @Autowired
    ISysContentServiceImpl contentService;

    @GetMapping("/hello")
    public PageList index() throws Exception {
        PageList<String> pageList =new PageList<String>(1,1,1);
        return pageList;
    }
    @GetMapping("/hello1")
    public String index2()
    {
        SysContent content =new SysContent();
        content.setContent("aaa");
        content.setDescription("bbb");
        content.setSign("ccc");
        boolean b = contentService.insert(content);

        return "Hello World12";
    }
    @GetMapping("/InsertAndDelete")
    public boolean InsertAndDelete() throws Exception {

        boolean b = contentService.InsertAndDelete();
        return b;
    }
    @GetMapping("/deleteall")
    public boolean deleteall()
    {

       boolean b = contentService.deleteAll();
        return b;
    }
    @ApiOperation(value="获取分页信息value", notes="获取分页信息notes")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "页面ID", required = true, dataType = "Long",paramType = "path")
    })
    @GetMapping("/page")
    public ReturnJsonType<Page<SysContent>> page(Long id)
    {
        logger.info("logger.info:pagegetma");
        logger.error("logger.error:pagegetma");
        Page<SysContent> pagelist = contentService.selectSysContentPage();
        ReturnJsonType<Page<SysContent>> returnJsonType =new ReturnJsonType<>();
        returnJsonType.setData(pagelist);
        returnJsonType.setMessage("OK");
        returnJsonType.setStatus(ExceptionType.Success);
        return returnJsonType;
    }

    @GetMapping("getarray")
    public ReturnJsonType<int[]> Demo1()
    {
        ReturnJsonType<int[]> returnJsonType =new ReturnJsonType<>();
        int [] r =new int[]{1,2,3,4};
        returnJsonType.setData(r);
        return returnJsonType;
    }


/*
    @RequestMapping("/selectall")
    public Content selectall()
    {
        Content list = contentService.selectById(1);
        return list;
    }*/
}
