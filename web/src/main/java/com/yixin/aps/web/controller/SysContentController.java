package com.yixin.aps.web.controller;

import com.yixin.aps.common.ExceptionType;
import com.yixin.aps.common.ReturnJsonType;
import com.yixin.aps.entity.SysContent;
import com.yixin.aps.service.impl.ISysContentServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by maming on 2018/6/7.
 */
@RestController
@RequestMapping("/Manage/SysContent")
public class SysContentController {

    @Autowired
    ISysContentServiceImpl sysContentService;

    @PostMapping("/Insert")
    public ReturnJsonType<String> insert(SysContent sysContent)
    {
        ReturnJsonType<String> r =new ReturnJsonType<>();
        String msg ="";
        if(StringUtils.isBlank(sysContent.getSign()))
        {
            msg += "Sign 不能为空";
        }
        if(StringUtils.isNotBlank(msg))
        {
            r.setStatus(ExceptionType.Fail);
            r.setMessage(msg);
            return r;
        }

        boolean b = sysContentService.insert(sysContent);
        if(b)
        {
            r.setStatus(ExceptionType.Success);
            r.setMessage("OK");

        }else
        {
            r.setStatus(ExceptionType.Fail);
            r.setMessage("新增失败");
        }
        return r;
    }

}
