package com.yixin.aps.mapper;

import com.yixin.aps.entity.SysContent;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author maming
 * @since 2018-05-24
 */
public interface SysContentDao extends BaseMapper<SysContent> {
    /**
     * 自定义注入方法
     */
    int deleteAll();
}
