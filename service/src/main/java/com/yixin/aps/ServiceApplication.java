package com.yixin.aps;

import org.springframework.boot.SpringApplication;
/*import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
////扫描common
@ComponentScan(basePackages = {"com.yixin.aps.**"})
//mybatis扫描
@MapperScan("com.yixin.aps.**")
//启注解事务管理
@EnableTransactionManagement*/
public class ServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceApplication.class, args);
	}
}
