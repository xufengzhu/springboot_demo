package com.yixin.aps.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author maming
 * @since 2018-05-24
 */
@TableName("sys_content")
public class SysContent extends Model<SysContent> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "Id", type = IdType.AUTO)
    private Integer Id;
    /**
     * 标记名称
     */
    private String Sign;
    /**
     * 标记描述
     */
    private String Description;
    /**
     * 标记内容
     */
    private String Content;


    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getSign() {
        return Sign;
    }

    public void setSign(String Sign) {
        this.Sign = Sign;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public static final String ID = "Id";

    public static final String SIGN = "Sign";

    public static final String DESCRIPTION = "Description";

    public static final String CONTENT = "Content";

    @Override
    protected Serializable pkVal() {
        return this.Id;
    }

    @Override
    public String toString() {
        return "SysContent{" +
        ", Id=" + Id +
        ", Sign=" + Sign +
        ", Description=" + Description +
        ", Content=" + Content +
        "}";
    }
}
