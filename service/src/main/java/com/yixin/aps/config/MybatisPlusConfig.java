package com.yixin.aps.config;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.plugins.PerformanceInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig {
    /**
     *   mybatis-plus分页插件 开启分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor page = new PaginationInterceptor();
        //page.setDialectType("mysql");
        return page;
    }
    /**
     * mybatis-plus SQL执行效率插件【生产环境可以关闭】
     * SQL 执行性能分析，开发环境使用，线上不推荐。 maxTime 指的是 sql 最大执行时长
     */
    @Bean
    public PerformanceInterceptor performanceInterceptor() {
            //<property name="maxTime" value="30000" />
            //<!--SQL是否格式化 默认false-->
            //<property name="format" value="true" />
        return new PerformanceInterceptor().setFormat(true);
    }
}