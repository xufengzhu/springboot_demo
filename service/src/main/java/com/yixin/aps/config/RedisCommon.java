package com.yixin.aps.config;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/8/15.
 */
@Component
public class RedisCommon {

    @Autowired
    RedisClient JedisCache;


    public Integer getExpireMax() {
        return expireMax;
    }

    /**
     * 最大过期时间
     */
    private Integer expireMax=Integer.MAX_VALUE;

    /**
     * 将hash保存进redis
     * @param key
     * @param value
     * @param <T>
     */
    public  <T extends Serializable> void cacheInsert(String key, Map<String,T> value){
        if(value!=null){
            JedisCache.setHash(key, value);
        }
    }

    /**
     * 将hash保存进redis ,指定过期时间
     * @param key
     * @param value
     * @param seconds
     * @param <T>
     */
    public  <T extends Serializable> void cacheInsert(String key,Map<String,T> value,Integer seconds){
        if(value!=null){
            JedisCache.setHash(key, value, seconds);
        }
    }

    /**
     * 返回整个hash
     * @param key
     * @return
     */
    public  <T extends Serializable> Map<String ,T> getCacheHash(String key){
        return JedisCache.getHash(key);
    }

    /**
     * 删除redis中key的field。
     * @param key
     * @param field
     * @return
     */
    public boolean removeCacheHash(String key,String field){
        return JedisCache.removeMap(key,field);
    }

    /**
     * 获取redis哈希表key中的域 field 的值。
     * @param key
     * @param field
     * @return
     */
    public <T extends Serializable> T getCacheHash(String key,String field){
        return JedisCache.getMap(key,field);
    }

    /**
     * 将redis哈希表 key 中的域 field 的值设为 value 。
     * @param key
     * @param field
     * @param value
     * @return
     */
    public  <T extends Serializable> boolean putHash(String key, String field, T value){
        return JedisCache.putMap(key,field,value);
    }

    public  <T extends Serializable> void cacheInsert(String key,List<T> value){
        if(value!=null){
            JedisCache.setList(key, value);
        }
    }

    /**
     * 默认缓存5分钟
     * @param key
     * @param value
     * @return
     */
    public  String cacheInsertStr(String key,String value){
        String r="";
        if(StringUtils.isNotBlank(value)){
            r=JedisCache.set(key, value);
        }
        return r;
    }

    public  String cacheInsertStr(String key,String value,Integer seconds){
        String r="";
        if(StringUtils.isNotBlank(value)){
            r=JedisCache.set(key, value, seconds);
        }
        return r;
    }

    /**
     * 如果是插入String 那么请用CacheInsertStr
     * @param key
     * @param t
     * @param <T>
     * @return
     */
    public  <T extends Serializable> String cacheInsert(String key,T t)
    {
        String r =JedisCache.setT(key, t);
        return r;
    }

    public  <T extends Serializable> String cacheInsert(String key,T t,int second)
    {
        String r =JedisCache.setT(key, t,second);
        return r;
    }

    public  <T extends Serializable> void cacheInsert(String key,List<T> value,Integer seconds){
        if(value!=null){
            JedisCache.setList(key, value, seconds);
        }
    }

    /**
     * 将可序列化对象添加到list缓存 的头部
     * @param key
     * @param v
     * @param <T>
     * @return
     */
    public <T extends Serializable> Long cacheListlpush(String key,T v)
    {
        return JedisCache.lpush(key,v);
    }
    /**
     * 将可序列化对象添加到list缓存 的尾部
     * @param key
     * @param v
     * @param <T>
     * @return
     */
    public <T extends Serializable> Long cacheListrpush(String key,T v)
    {
        return JedisCache.rpush(key,v);
    }

    /**
     * 取出某个区间内的list缓存
     * @param key
     * @param start
     * @param end
     * @param <T>
     * @return
     */
    public  <T extends Serializable> List<T> getCacheListByIndex(String key, int start, int end){
        return JedisCache.getListByIndex( key,  start,  end);
    }

    public  boolean hasCache(String key){
        return JedisCache.existsKey(key);
    }

    /**
     * 移除缓存
     * @param key
     */
    public  Long removeCache(String key){
        Long r = JedisCache.delete(key);
        return r;
    }

    /**
     * 返回String 如果返回 T用 GetCacheT
     * @param key
     * @return
     */
    public  String getCacheStr(String key){
        return JedisCache.get(key);
    }

    /**
     * 返回T  如果返回String 用GetCacheStr
     * @param key
     * @return
     */
    public  <T extends Serializable> T getCacheT(String key){
        return JedisCache.getT(key);
    }

    /**
     * 返回整个List
     * @param key
     * @return
     */
    public  <T extends Serializable> List<T> getCacheList(String key){
        return JedisCache.getListAll(key);
    }

}
