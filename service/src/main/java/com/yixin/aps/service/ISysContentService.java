package com.yixin.aps.service;

import com.yixin.aps.entity.SysContent;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author maming
 * @since 2018-05-24
 */
public interface ISysContentService extends IService<SysContent> {

    boolean deleteAll();
}
