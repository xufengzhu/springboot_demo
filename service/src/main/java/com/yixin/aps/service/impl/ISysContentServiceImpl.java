package com.yixin.aps.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yixin.aps.config.RedisCommon;
import com.yixin.aps.entity.SysContent;
import com.yixin.aps.mapper.SysContentDao;
import com.yixin.aps.service.ISysContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author maming
 * @since 2018-05-24
 */
@Service
public class ISysContentServiceImpl extends ServiceImpl<SysContentDao, SysContent> implements ISysContentService {

    @Autowired
    RedisCommon redisCommon;

    @Override
    public boolean deleteAll() {
        return retBool(baseMapper.deleteAll());
    }
/*    解决方案一：手动回滚。给注解加上参数如：@Transactional(rollbackFor=Exception.class)

    解决方案二：如上述分析。MyException改为继承RuntimeException的异常。并且在service上层要继续捕获这个异常并处理

    解决方案三：在service层方法的catch语句中增加：TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();语句，手动回滚，这样上层就无需去处理异常
   */
    @Transactional(rollbackFor=Exception.class)
    public boolean InsertAndDelete() throws Exception {

        SysContent m2 =new SysContent();
        m2.setContent("aaa");
        m2.setDescription("bbb");
        m2.setSign("bb");
        m2.insert();
        SysContent m =new SysContent();
        m.setContent("aaa");
        m.setDescription("bbb");
        m.setSign("aa");
        boolean bInsert = m.insert();
        EntityWrapper ew=new EntityWrapper();
        ew.setEntity(new SysContent());
        ew.where("sign={0}","aa");
        delete(ew);
        //deleteAll();
        throw new Exception();
        //return true;
    }

    public Page<SysContent> selectSysContentPage() {
        // 不进行 count sql 优化，解决 MP 无法自动优化 SQL 问题
        // page.setOptimizeCountSql(false);
        // 不查询总记录数
        // page.setSearchCount(false);
        Page<SysContent> page =new Page<SysContent>();
        page.setSearchCount(true);
        page.setSize(2);
        page.setCurrent(1);

        Page<SysContent> pageList =this.selectPage(page);
        //redisCommon.cacheInsert("key2",pageList);
        //Page<SysContent> pageListT = (Page<SysContent>)redisCommon.getCacheT("key2");
        return pageList;
    }
}
