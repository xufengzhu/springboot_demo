package com.yixin.aps.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.yixin.aps.entity.SysContent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by maming on 2018/5/24.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ISysContentServiceImplTest {

    @Autowired
    ISysContentServiceImpl sysContentService;

    @Test
    public void selectSysContentPage() throws Exception {
      Page<SysContent> pagelist = sysContentService.selectSysContentPage();
    }

}